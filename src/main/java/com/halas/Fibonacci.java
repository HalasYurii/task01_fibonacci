package com.halas;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/**
 * Program created by @author Yurii Halas.
 * <p>
 * destination is print fibonacci numbers
 * <p>
 * 22.01.2019 0:33
 */
public final class Fibonacci {
    /**
     * constructor that can't be called.
     */
    private Fibonacci() {
    }

    /**
     * function will give you
     * true if this number the Fibonacci or false if not.
     *
     * @param someValue integer digit that will be checked
     * @return boolean type false or true about digit
     */
    public static boolean isFibonacci(final int someValue) {
        int first = 1;
        int second = 0;
        for (int i = 0; i < someValue; i++) {
            first += second;
            second = first - second;

            if (first > someValue) {
                return false;
            } else if (first == someValue) {
                return true;
            }
        }
        return false;
    }

    /**
     * this function will check if interval is correct.
     *
     * @param first  is value of the start of the interval
     * @param second is value of the end of the interval
     * @return boolean type if digit has correct interval and return false
     * or if not correct then true
     */
    public static boolean isNotCorrect(final int first, final int second) {
        return (first > second) || (first < 0);
    }

    /**
     * function will set all Fibonacci digits from some interval from
     * Minimum value to Maximum and save the result in List
     * in instance list.
     *
     * @param from is start value of the interval
     * @param to   is end value of the interval
     * @param list is instance of List and saves all fibonacci
     *             digits from the interval
     */
    public static void setToArrayFibonacciFromMinToMax(
            final int from,
            final int to,
            final List<Integer> list) {
        if (isNotCorrect(from, to)) {
            return;
        }
        for (int i = from; i <= to; i++) {
            if (isFibonacci(i)) {
                list.add(i);
            }
        }
    }

    /**
     * function will set all Fibonacci digits from some interval from
     * Maximum value to Minimum and save the result in List
     * in instance list.
     *
     * @param from is start value of the interval
     * @param to   is end value of the interval
     * @param list is instance of List and saves all fibonacci
     *             digits from the interval
     */
    public static void setToArrayFibonacciFromMaxToMin(
            final int from,
            final int to,
            final List<Integer> list) {
        if (isNotCorrect(from, to)) {
            return;
        }
        for (int i = to; i >= from; i--) {
            if (isFibonacci(i)) {
                list.add(i);
            }
        }

    }

    /**
     * function will set all odd fibonacci digits from some interval from
     * start to end interval and save the result in List
     * in instance list.
     *
     * @param start is start value of the interval
     * @param end   is end value of the interval
     * @param list  is instance of List and saves all odd fibonacci
     *              digits from the interval
     */
    public static void setOddFibonacciNumbersFromStart(
            final int start,
            final int end,
            final List<Integer> list) {
        if (isNotCorrect(start, end)) {
            return;
        }
        for (int i = start; i <= end; i++) {
            if ((i % 2 != 0) && (isFibonacci(i))) {
                list.add(i);
            }
        }
    }

    /**
     * function will set all odd fibonacci digits from some interval from
     * end interval to start and save the result in List
     * in instance list.
     *
     * @param start is start value of the interval
     * @param end   is end value of the interval
     * @param list  is instance of List and saves all odd fibonacci
     *              digits from the interval
     */
    public static void setOddFibonacciNumbersFromEnd(
            final int start,
            final int end,
            final List<Integer> list) {
        if (isNotCorrect(start, end)) {
            return;
        }
        for (int i = end; i >= start; i--) {
            if ((i % 2 != 0) && (isFibonacci(i))) {
                list.add(i);
            }
        }
    }


    /**
     * function will find the biggest odd Fibonacci digit from
     * interval.
     *
     * @param from is start value of the interval
     * @param to   is end value of the interval
     * @return if zero then mistake in the interval or can't find
     * if something else then it's the biggest odd fibonacci digit
     * from the interval
     */
    public static int findTheBigestFibOddNumber(
            final int from,
            final int to) {

        if (isNotCorrect(from, to)) {
            return 0;
        }
        for (int i = to; i >= from; i--) {
            if ((i % 2 != 0) && (isFibonacci(i))) {
                return i;
            }
        }
        return 0;

    }

    /**
     * function will find the biggest even Fibonacci digit from
     * interval.
     *
     * @param from is start value of the interval
     * @param to   is end value of the interval
     * @return if zero then mistake in the interval or can't find
     * if something else then it's the biggest even fibonacci digit
     * from the interval
     */
    public static int findTheBigestFibEvenNumber(
            final int from,
            final int to) {

        if (isNotCorrect(from, to)) {
            return 0;
        }
        for (int i = to; i >= from; i--) {
            if ((i % 2 == 0) && (isFibonacci(i))) {
                return i;
            }
        }
        return 0;

    }

    /**
     * function will return the sum of the all odd fibonacci digits.
     *
     * @param start is start value of the interval
     * @param end   is end value of the interval
     * @return if zero then mistake in the interval or
     * have't odd fibonacci digits, if something else
     * then it's sum of the all odd fibonacci digits from the interval
     */
    public static int getSumAllOddFibonacciNumbers(
            final int start,
            final int end) {
        if (isNotCorrect(start, end)) {
            return 0;
        }
        int sum = 0;
        for (int i = start; i <= end; i++) {
            if ((i % 2 != 0) && (isFibonacci(i))) {
                sum += i;
            }
        }
        return sum;
    }


    /**
     * function will return the sum of the all even fibonacci digits.
     *
     * @param start is start value of the interval
     * @param end   is end value of the interval
     * @return if zero then mistake in the interval or
     * have't odd fibonacci digits, if something else
     * then it's sum of the all even fibonacci digits from the interval
     */
    public static int getSumAllEvenFibonacciNumbers(
            final int start,
            final int end) {
        if (isNotCorrect(start, end)) {
            return 0;
        }
        int sum = 0;
        for (int i = start; i <= end; i++) {
            if ((i % 2 == 0) && (isFibonacci(i))) {
                sum += i;
            }
        }
        return sum;
    }

    /**
     * function will return the percent in double value.
     *
     * @param start is start value of the interval
     * @param end   is end value of the interval
     * @return if zero then there is not odd fibonacci digits, if something else
     * then it's percent of the all odd fibonacci digits from the interval
     */
    public static double getPercentOfOddFibNumbers(
            final int start,
            final int end) {
        int sizeAllFib = 0;
        int sizeOddFib = 0;
        final int toPercent = 100;

        for (int i = start; i <= end; i++) {
            if (isFibonacci(i)) {
                sizeAllFib++;
                if (i % 2 != 0) {
                    sizeOddFib++;
                }
            }
        }
        if (sizeAllFib == 0) {
            return 0;
        }
        return (sizeOddFib * toPercent * 1.) / sizeAllFib;
    }


    /**
     * function will return the percent in double value.
     *
     * @param start is start value of the interval
     * @param end   is end value of the interval
     * @return if zero then there is not even fibonacci digits,
     * if something else then it's percent of the all even
     * fibonacci digits from the interval
     */
    public static double getPercentOfEvenFibNumbers(
            final int start,
            final int end) {
        int sizeAllFib = 0;
        int sizeEvenFib = 0;
        final int toPercent = 100;

        for (int i = start; i <= end; i++) {
            if (isFibonacci(i)) {
                sizeAllFib++;
                if (i % 2 == 0) {
                    sizeEvenFib++;
                }
            }
        }
        if (sizeAllFib == 0) {
            return 0;
        }
        return (sizeEvenFib * toPercent * 1.) / sizeAllFib;
    }

    /**
     * main function.
     *
     * @param args is param of the console
     */
    public static void main(final String[] args) {
        try {
            final Scanner scanner = new Scanner(System.in);
            System.out.println("Enter the start value of your interval: ");
            final int from = scanner.nextInt();
            System.out.println("Enter the end value of your interval: ");
            final int to = scanner.nextInt();


            final ArrayList<Integer> list = new ArrayList<Integer>();

            setToArrayFibonacciFromMinToMax(from, to, list);
            System.out.println("Fibonacci from start to end: " + list);
            list.clear();


            setToArrayFibonacciFromMaxToMin(from, to, list);
            System.out.println("Fibonacci from end to start: " + list);
            list.clear();


            final int biggestOdd = findTheBigestFibOddNumber(from, to);
            System.out.println("The biggest odd number from Fibonacci is: ");
            if (biggestOdd == 0) {
                System.out.println("does not exist!");
            } else {
                System.out.println(biggestOdd);
            }

            final int biggestEven = findTheBigestFibEvenNumber(from, to);
            System.out.println("The biggest even number from Fibonacci is: ");
            if (biggestEven == 0) {
                System.out.println("does not exist!");
            } else {
                System.out.println(biggestEven);
            }

            setOddFibonacciNumbersFromStart(from, to, list);
            System.out.println("Odd Fibonacci numbers from start: " + list);
            list.clear();

            setOddFibonacciNumbersFromEnd(from, to, list);
            System.out.println("Odd Fibonacci numbers from end: " + list);
            list.clear();

            System.out.println("Sum of all odd Fibonacci digits: "
                    + getSumAllOddFibonacciNumbers(from, to));
            System.out.println("Sum of all even Fibonacci digits: "
                    + getSumAllEvenFibonacciNumbers(from, to));

            final double percentOddFibDigits
                    = getPercentOfOddFibNumbers(from, to);
            final double percentEvenFibDigits
                    = getPercentOfEvenFibNumbers(from, to);

            System.out.println("Percent of all odd Fibonacci digits: "
                    + percentOddFibDigits);
            System.out.println("Percent of all even Fibonacci digits: "
                    + percentEvenFibDigits);
        } catch (InputMismatchException e) {
            System.out.println("Error input!!!");
        }
    }
}
